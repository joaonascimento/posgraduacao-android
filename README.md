# posgraduacao-android

Projeto para a aula de Android desenvolvido em Kotlin.

**Faculdade:** Faculdades Integradas de Bauru

**Aluno:** João Paulo Nascimento

**Ambiente de Desenvolvimento**

- **Desenvolvido no:** Android Studio 4.1.1
- **Android SDK:** v30
- **Versão do Kotlin:** 1.4.21-release-Studio4.1.1

